import hashlib
from modulo import login
eml = str(input("Informe o seu email:"))
snh = str(input("Informe a senha do usuário:"))
if login(eml,snh):
    email = hashlib.md5(eml.encode())
    senha = hashlib.md5(snh.encode())
    print("Usuário conseguiu logar.")
    print("O equivalente do email hexadecimal de hash é:" + email.hexdigest())
    print("O equivalente da senha hexadecimal de hash é:" + senha.hexdigest())
else:
    print("Usuário NÃO conseguiu logar.")
